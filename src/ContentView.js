import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {WebView} from 'react-native-webview';

function ContentView() {
  const webViewRef = useRef(null);
  const goback = () => {
    webViewRef.current.goBack();
  };
  const [loading, setLoading] = useState(false);
  const [loadingLogo, setLoadingLogo] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setLoadingLogo(false);
    }, 5000);
  });
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style={styles.barHeight} />
      <View style={styles.navbar}>
        <TouchableOpacity onPress={goback}>
          <View style={styles.back}>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                tintColor: 'black',
              }}
              source={require('./assets/chevron-left.png')}
            />
          </View>
        </TouchableOpacity>
      </View>
      {loadingLogo ? (
        <View style={styles.ActivityIndicatorStyle}>
          <Image
            style={{
              width: 100,
              height: 100,
              resizeMode: 'contain',
              marginVertical: 10,
              borderRadius: 100,
            }}
            source={require('./assets/ic_launcher.png')}
          />
        </View>
      ) : null}
      {loading ? (
        <View style={styles.ActivityIndicatorStyle}>
          {/* // style={{width: '100%', alignItems: 'center', flex: 1}}> */}

          <ActivityIndicator color="grey" size="large" />
        </View>
      ) : null}
      <WebView
        ref={webViewRef}
        source={{uri: 'https://phsarfc.ebuycambo.com/'}}
        onLoadStart={() => setLoading(true)}
        onLoad={() => setLoading(false)}
      />
    </SafeAreaView>
  );
}

export default ContentView;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    // alignItems: 'center',
    justifyContent: 'center',
  },
  barHeight: {
    paddingTop: Platform.OS === 'ios' ? StatusBar.currentHeight : 0,
  },
  navbar: {
    height: 40,
    width: '100%',
    // flexDirection: 'row-reverse',
    paddingTop: 6,
    backgroundColor: '#fefefe',
    borderTopColor: '#f1f1f1',
    borderTopWidth: 1,
  },
  back: {
    width: 50,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  forward: {
    width: 50,
    height: 50,
  },
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 99,
    alignItems: 'center',
    width: '100%',
  },
});
