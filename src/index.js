import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, StatusBar } from 'react-native'

import ContentView from './ContentView';
function index(props) {
    return (
        <View style={styles.container}>
            <ContentView/>
        </View>
    );
}

export default index;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
})